package com.example.mylocation;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.mylocation.model.GoogleMapsResponse;
import com.example.mylocation.tool.GoogleMapsManager;
import com.example.mylocation.tool.IGoogleMapsRequester;
import com.example.mylocation.tool.PermissionManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, IGoogleMapsRequester {

    @Bind(R.id.searchView)
    SearchView searchView;

    private GoogleMap mMap;
    private GoogleMapsManager googleMapsManager;
    private ProgressDialog progressDialog;
    private GoogleMapsResponse googleMapResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        googleMapsManager = new GoogleMapsManager(this);
        mapFragment.getMapAsync(this);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                googleMapsManager.sendAddressRequest(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (PermissionManager.checkLocationPermission(this)) {
            onPermissionGranted();
        }
    }

    private boolean checkLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void enableLocation() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("GPS non attivo")
                .setMessage("Il GPS non è attivo, vuoi attivarlo?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void onPermissionGranted() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        final Activity activity = this;
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onPermissionGranted();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Permesso localizzazione");
                builder.setMessage("Per facilitare l'inserimento dell'indirizzo, è necessario autorizzare la possibilità di localzzarti, vuoi autorizzare?");
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                Snackbar.make(findViewById(android.R.id.content), "Errore", Snackbar.LENGTH_LONG)
                        .setAction(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PermissionManager.moveToAppSettings(activity);
                            }
                        })
                        .setActionTextColor(Color.RED)
                        .show();
            }
        }
    }


    @Override
    public boolean onMyLocationButtonClick() {
        if (checkLocationEnabled()) {
            final Context context = this;

            FusedLocationProviderClient mfusedLocationProviderclient = new FusedLocationProviderClient(this);

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
            mfusedLocationProviderclient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(android.location.Location location) {
                    if (location != null) {
                        googleMapsManager.sendLatlngRequest(location);
                    } else {
                        Toast.makeText(context, "Non è stato possibile rilevare la posizione, riprova!", Toast.LENGTH_LONG).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(context, "Non è stato possibile rilevare la posizione GPS...", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            enableLocation();
        }
        return false;

    }

    @Override
    public void onStartRequest() {
        progressDialog = ProgressDialog.show(this, "Attendi ...", "Ricerca indirizzo ...", true);
    }


    @Override
    public void onSuccess(GoogleMapsResponse googleMapResponse) {
        progressDialog.dismiss();
        this.googleMapResponse = googleMapResponse;
        showLocation(googleMapResponse);
    }

    @Override
    public void onFailure(String message) {
        progressDialog.dismiss();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void showLocation(GoogleMapsResponse googleMapResponse) {
        if (googleMapResponse.getResults().size() != 0) {
            double lat = googleMapResponse.getResults().get(0).getGeometry().getLocation().getLat();
            double lng = googleMapResponse.getResults().get(0).getGeometry().getLocation().getLng();
            LatLng location = new LatLng(lat, lng);
            String address = googleMapResponse.getResults().get(0).getFormattedAddress();
            Marker marker = mMap.addMarker(new MarkerOptions().position(location).title(address));
            marker.showInfoWindow();
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 14));
        } else {
            Toast.makeText(this, "Nessun location valida è stata trovata", Toast.LENGTH_LONG).show();
        }
    }
}
