package com.example.mylocation.tool;

import android.location.Location;

import com.example.mylocation.model.GoogleMapsResponse;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;



public class GoogleMapsManager {

    private AsyncHttpClient client;
    private Gson gson;
    private IGoogleMapsRequester handler;
    private static String URL = "https://maps.googleapis.com/maps/api/geocode/json?";

    public GoogleMapsManager(IGoogleMapsRequester handler) {
        this.handler = handler;
        client = new AsyncHttpClient();
        gson = new Gson();
    }

    public void sendAddressRequest(String query) {
        String param = "address=" + addressToSend(query);
        sendRequest(param, handler);
    }

    public void sendLatlngRequest(Location location) {
        String param = "latlng=" + location.getLatitude() + "," + location.getLongitude();
        sendRequest(param, handler);
    }

    private void sendRequest(String params, final IGoogleMapsRequester handler) {

        client.get(URL + params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                handler.onStartRequest();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                GoogleMapsResponse googleMapsResponse = gson.fromJson(response.toString(), GoogleMapsResponse.class);
                handler.onSuccess(googleMapsResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                handler.onFailure("Errore");
            }
        });
    }

    private String addressToSend(String text) {
        return text.replaceAll(" ", "%20");
    }

}

