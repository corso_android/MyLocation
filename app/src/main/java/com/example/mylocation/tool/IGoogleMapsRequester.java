package com.example.mylocation.tool;


import com.example.mylocation.model.GoogleMapsResponse;

public interface IGoogleMapsRequester {

    void onStartRequest();

    void onSuccess(GoogleMapsResponse googleMapsResponse);

    void onFailure(String message);
}